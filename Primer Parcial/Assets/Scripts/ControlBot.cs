using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlBot : MonoBehaviour
{

    private GameObject Jugador;
    public float rapidez;

    // Start is called before the first frame update
    void Start()
    {
        Jugador = GameObject.Find("Jugador");
    }

    // Update is called once per frame
    private void Update()
    {
        transform.LookAt(Jugador.transform);
        transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
    }
}
