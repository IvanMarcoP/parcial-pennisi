using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlJugador : MonoBehaviour
{
    private Rigidbody rb;
    public float rapidez = 1.5f;
    public float alturaSalto = 9.8f;
    public LayerMask capaPiso; 
    public SphereCollider col;
    private int cont;
    public TMPro.TMP_Text textoCantidadRecolectados;
    public TMPro.TMP_Text textoGanaste;
    public bool puedeDobleSalto;
    public float distanciaPiso = 1;
    //public float rapidezDesplazamiento = 10.0f;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        col = GetComponent<SphereCollider>();
        cont = 0;
        textoGanaste.text = "";
        setearTextos();

    }

    private void setearTextos()
    {
        textoCantidadRecolectados.text = "Cantidad recolectados: " + cont.ToString() + " / 10";
        if (cont >= 10)
        {
            textoGanaste.text = "Ganaste!";
            Time.timeScale = 0;
        }
    }

    // Update is called once per frame
    void Update()
    {
        float movimientoHorizontal = Input.GetAxis("Horizontal");
        float movimientoVertical = Input.GetAxis("Vertical");

        Vector3 vectorMovimiento = new Vector3(movimientoHorizontal, 0.0f, movimientoVertical);

        rb.AddForce(vectorMovimiento * rapidez);


        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (EstaEnPiso())
            {
                rb.AddForce(Vector3.up * alturaSalto, ForceMode.Impulse);
                puedeDobleSalto = true;
            }
            else if (puedeDobleSalto)
            {
                rb.AddForce(Vector3.up * alturaSalto, ForceMode.Impulse);
                puedeDobleSalto = false;
            }

        }

    }



    private bool EstaEnPiso()
    {
        //return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x, col.bounds.min.y, col.bounds.center.z), col.radius * .9f, capaPiso);
        return Physics.Raycast(transform.position, Vector3.down, distanciaPiso);
    }


    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.CompareTag("Coleccionable") == true)
        {
            cont = cont + 1;
            setearTextos();
            other.gameObject.SetActive(false);
        }

        if (other.gameObject.CompareTag("Secreto") == true)
        {
            rb.transform.position = new Vector3(-66f, 1.2f, -25f);
            other.gameObject.SetActive(false);
        }

        if (other.gameObject.CompareTag("Ganar") == true)
        {
            cont = cont + 9;
            setearTextos();
            other.gameObject.SetActive(false);
        }

        if (other.gameObject.CompareTag("Boost") == true)
        {
            rapidez = rapidez * 2;
            other.gameObject.SetActive(false);
        }

        if (other.gameObject.CompareTag("Debuff") == true)
        {
            rapidez = rapidez / 2;
            other.gameObject.SetActive(false);
        }

        if (other.gameObject.CompareTag("Bot") == true)
        {
            rb.transform.position = new Vector3(0f, 1f, 0f);
        }

        if (other.gameObject.CompareTag("Respawn") == true)
        {
            rb.transform.position = new Vector3(0f, 1f, 0f);
        }

        if (other.gameObject.CompareTag("Tama�o") == true)
        {
            rb.transform.localScale = new Vector3(2f, 2f, 2f);
            distanciaPiso = 1.4f;

            if (Input.GetKeyDown(KeyCode.Space))
            {
                if (EstaEnPiso())
                {
                    rb.AddForce(Vector3.up * alturaSalto, ForceMode.Impulse);
                    puedeDobleSalto = true;
                }
                else if (puedeDobleSalto)
                {
                    rb.AddForce(Vector3.up * alturaSalto, ForceMode.Impulse);
                    puedeDobleSalto = false;
                }

            }

            other.gameObject.SetActive(false);

        }
    }
}
