using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlJuego : MonoBehaviour
{

    public TMPro.TMP_Text textoTiempo;
    public TMPro.TMP_Text textoGameOver;
    public TMPro.TMP_Text textoReiniciar;
    float tiempoRestante;

    // Start is called before the first frame update
    void Start()
    {
        Reanudar();
        textoReiniciar.text = "";
        textoGameOver.text = "";
        setearTexto();
        ComenzarJuego();
    }

    private void gameOver()
    {
        textoGameOver.text = "Game Over";
        textoReiniciar.text = "Presiona la tecla R para reiniciar";
    }

    private void setearTexto ()
    {
        textoTiempo.text = "Tiempo Restante: " + tiempoRestante + " segundos";
    }

    // Update is called once per frame
    void Update()
    {
        if (tiempoRestante == 0)
        {
            gameOver();
            Pausar();
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            //rb.transform.position = new Vector3(0f, 1f, 0f);
            SceneManager.LoadScene("proyecto");
        }

    }


    public void Pausar()
    {
        Time.timeScale = 0;
    }

    public void Reanudar()
    {
        Time.timeScale = 1;
    }

    void ComenzarJuego()
    {
        StartCoroutine(ComenzarCronometro(60));
    }

    public IEnumerator ComenzarCronometro(float valorCronometro = 60)
    {
        tiempoRestante = valorCronometro;
        while (tiempoRestante > 0)
        {
            setearTexto();
            yield return new WaitForSeconds(1.5f);
            tiempoRestante--;
        }
    }
}
