using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlBotSalta : MonoBehaviour
{
    private Rigidbody rb;
    private GameObject Jugador;
    public float rapidez;
    public float distanciaPiso = 1;
    public bool puedeSaltoAire;
    public float alturaSalto = 5.5f;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        Jugador = GameObject.Find("Jugador");
    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(Jugador.transform);
        transform.Translate(rapidez * Vector3.forward * Time.deltaTime);


        if (Input.GetKeyDown(KeyCode.Space))
        {

            if (EstaEnPiso())
            {
                rb.AddForce(Vector3.up * alturaSalto, ForceMode.Impulse);
                puedeSaltoAire = true;
            }
            else if (puedeSaltoAire)
            {
                rb.AddForce(Vector3.up * alturaSalto, ForceMode.Impulse);
                puedeSaltoAire = false;
            }
        }
    }

    private bool EstaEnPiso()
    {
        //return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x, col.bounds.min.y, col.bounds.center.z), col.radius * .9f, capaPiso);
        return Physics.Raycast(transform.position, Vector3.down, distanciaPiso);
    }

}
