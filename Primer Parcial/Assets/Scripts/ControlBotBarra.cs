using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlBotBarra : MonoBehaviour
{
    public float rapidez = .5f; 
    public float strength = 15f;

    private float randomXOffset;

    // Start is called before the first frame update
    void Start()
    {
        randomXOffset = Random.Range(-9f, 10f);
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 pos = transform.position; 
        pos.x = Mathf.Sin(Time.time * rapidez + randomXOffset) * strength; 
        transform.position = pos;
    }
}
